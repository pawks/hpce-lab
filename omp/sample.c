#include<stdio.h>
#include<omp.h>
#include<time.h>

int main()
{
    // int tnum,numts;
    // numts = omp_get_num_threads();
    # pragma omp parallel
    {
        int tnum,numts;
        numts = omp_get_num_threads();
        tnum = omp_get_thread_num();
        sleep(3);
        printf("%d of %d\n", tnum,numts);
    }
    return 0;
}