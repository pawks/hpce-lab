# Exercise 1

## Identify your Persistent Storage Vendor, Storage Type, Capacity, Maximum Sequential Read/Write Rate and Maximum Random Read/Write Rate.
- Vendor - Samsung
- Storage Type - HDD
- Capacity - 1 TB
- Maximum Sequential Read Rate - 87.5 MB/s
- Maximum Sequential Write Rate - 62.7MB/s
- Maximum Random Read Rate - 63.7kB/s
- Maximum Random Write Rate - 67.4kB/s

## Find RPM, Seek Time, Rotational Delay,  Transfer Time , and any other access time related parameters.Compute the access time for 10 MB data based on this specification.
- RPM - 5400
- Seek Time - 12 ms
- Rotational Delay - 5ms
- Transfer Time - 0.7 ms
- Access time - 17ms

## MTBF - specs
63 Years

## Processor Configuration - lscpu
- Number of cores - 2 
- Vendor - Intel
- Model - i7 5th gen 
- Operating Frequency - 2.4 GHz burst frequency - 3 GHz
- Core - 2
- Threads per core - 2
- Logical CPUs - 4

## RAM - sudo dmidecode --type 17
- size - 8GB
- type - DDR3

## Display Adapter
- Vendor - Intel
- Model Number - HD Graphics 5500

## Commercial processor
- 32 cores AMD EPYC
