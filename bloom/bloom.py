#!/usr/bin/env python3

# 3rd party
import mmh3
import random
from bitarray import bitarray

class BloomFilter(set):

    def __init__(self, size, hash_count):
        super(BloomFilter, self).__init__()
        self.bit_array = bitarray(size)
        self.bit_array.setall(0)
        self.size = size
        self.hash_count = hash_count

    def __len__(self):
        return self.size

    def __iter__(self):
       return iter(self.bit_array)

    def add(self, item):
        for ii in range(self.hash_count):
            index = mmh3.hash(item, ii) % self.size
            self.bit_array[index] = 1

        return self

    def __contains__(self, item):
        out = True
        for ii in range(self.hash_count):
            index = mmh3.hash(item, ii) % self.size
            if self.bit_array[index] == 0:
                out = False

        return out

def main():
    fbloom = BloomFilter(4464, 10)
    farr = []
    with open("f.txt","r") as ffile:
        farr = (ffile.read()).split()[0:500]
        for entry in farr:
            fbloom.add(entry)
    mbloom = BloomFilter(500, 2)
    marr = []
    with open("m.txt","r") as ffile:
        marr = (ffile.read()).split()[0:500]
        for entry in marr:
            mbloom.add(entry)
    
    print("Test for Female names")
    for test in [farr[i] for i in [random.randrange(0,500,random.randint(2,10)) for _ in range(10)]]:
        if test in fbloom:
            print('{} is in bloom filter as expected'.format(test))
        else:
            print('Something is terribly went wrong for {}'.format(tests))
            print('FALSE NEGATIVE!')

    print("Test for Male names")
    for test in [marr[i] for i in [random.randrange(0,500,random.randint(2,10)) for _ in range(10)]]:
        if test in mbloom:
            print('{} is in bloom filter as expected'.format(test))
        else:
            print('Something is terribly went wrong for {}'.format(tests))
            print('FALSE NEGATIVE!')
    
    print()
    for entry in marr+farr:
        if entry in fbloom and entry in mbloom:
            print("{} entry seems to be present in both.".format(entry))
    
    print()
    print("False positives")

    for other_name in [marr[i] for i in [random.randrange(0,500,random.randint(2,10)) for _ in range(10)]]:
        if other_name in fbloom:
            print('{} is not in the bloom, but a false positive'.format(other_name))
    
    for other_name in [farr[i] for i in [random.randrange(0,500,random.randint(2,10)) for _ in range(10)]]:
        if other_name in mbloom:
            print('{} is not in the bloom, but a false positive'.format(other_name))

    exit()

if __name__ == '__main__':
    main()