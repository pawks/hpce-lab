import socket
import sys
import argparse
parser = argparse.ArgumentParser()
parser.add_argument('-v',action='store',type=int)
args = parser.parse_args()
# Create a TCP/IP socket
sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# Connect the socket to the port where the server is listening
server_address = ('0.0.0.0', 5252)
print ('connecting to %s port %s' % server_address)
sock.connect(server_address)

try:
    message = bytes([args.v])
    print ('sending "%s"' % message)
    sock.sendall(message)
    code = ''
    data = sock.recv(32)
    code+=str(int.from_bytes(data,byteorder=sys.byteorder))
    print ('code "%s"' % code)
finally:
    print ('closing socket')
    sock.close()