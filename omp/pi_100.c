#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#ifndef NUM_THREADS
#define NUM_THREADS 8
#endif
#ifndef steps
#define steps 1000000000    
#endif
double step;

int main (int argc, const char *argv[]) {

    int i,j;
    double x;
    double sum = 0.0;
    double start;

    step = 1.0/(double) steps;

    printf(" running on %d threads\n", NUM_THREADS);
    double time=0.0,epi=0.0;
    // Compute parallel compute times for 1-MAX_THREADS
    for (j=1; j<= 100; j++) {

        

        // This is the beginning of a single PI computation 
        omp_set_num_threads(NUM_THREADS);

        sum = 0.0;
        double start = omp_get_wtime();


        #pragma omp parallel for reduction(+:sum) private(x)
        for (i=0; i < steps; i++) {
            x = (i+0.5)*step;
            sum += 4.0 / (1.0+x*x); 
        }

        // Out of the parallel region, finialize computation
        epi += step * sum;
        time += omp_get_wtime() - start;

    }
    printf("PI = %.16g computed in %.4g seconds\n", epi/100, time/100);
}