import os
import multiprocessing as mp
import threading
import sys
import shutil
from shutil import copyfile
import socket
import argparse
import time

def copy(ver):
    try:
        copyfile("./testfile","./temp/version-"+str(ver))
    except:
        return [404]
    else:
        return [200]

def serve(connection,client_address,start):
    try:
        print('connection from'+ str(client_address))
        while True:
            recv = connection.recv(32)
            print(recv)
            data = int.from_bytes(recv,byteorder=sys.byteorder)
            print ('version "%s"' % data)
            print ('copy')
            val = copy(str(data))
            print(val)
            connection.sendall(bytes(val))
            break
    finally:
        connection.close()
        print("REsponse:"+str(time.process_time_ns()-start)+"ns")
    return
if __name__ == "__main__":
    copyfile("/home/dracarys/Downloads/testfile","./testfile")
    if not os.path.exists("./temp"):
        os.mkdir("./temp")
    else:
        shutil.rmtree("./temp")
        os.mkdir("./temp")
    parser = argparse.ArgumentParser()
    parser.add_argument('--mp',action='store_true')
    parser.add_argument('--mt',action='store_true')
    args = parser.parse_args()
    sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_address = ('0.0.0.0', 5252)
    print('starting up on %s port %s' % server_address)
    sock.bind(server_address)
    sock.listen(2)
    while True:
        start = time.process_time_ns()
        print('waiting for a connection')
        connection, client_address = sock.accept()
        if args.mp:
            mp.Process(target=serve,args=(connection,client_address,start)).start()
        elif args.mt:
            threading.Thread(target=serve,args=(connection,client_address,start)).start()
        else:
            serve(connection,client_address,start)
